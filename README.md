# Igbinary

This package provides different serializers that could be used as drop-in
replacement for the serializers shipped with core:

- `serialization.igbinary`: Serializes using igbinary library.
- `serialization.igbinary_gz`: Serializes using igbinary library and compresses
  with zlib library.
- `serialization.phpserialize_gz`: Serializes using PHP functions and compresses
  with zlib library.


## Requirements

This module requires the following libraries:
- [igbinary](https://github.com/igbinary/igbinary)
- [zlib](https://www.php.net/zlib)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

The example.services.yml from the module will replace the keyvalue stores,
database and redis cache backends. Either include it directly or copy the
desired service definitions into a site specific services.yml file for more
control.

`$settings['container_yamls'][] = 'modules/contrib/igbinary/example.services.yml';`

You can avoid to have to enable the module by directly including the
igbinary.services.yml.

`$settings['container_yamls'][] = 'modules/contrib/igbinary/igbinary.services.yml';`

See the provided `example.services.yml` file on how to override the
serialization services used by various other services.


## Configuration

The module has no menu or modifiable settings. There is no configuration.
